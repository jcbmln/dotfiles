# Jacob Malone's Dotfiles

My dotfiles for `zsh`.

## Installation

You can install my dotfiles using curl:

```sh
sh -c "`curl -fsSL https://gitlab.com/jcbmln/dotfiles/raw/master/install.sh`"
```

or, using wget:

```sh
sh -c "`wget -O - --no-check-certificate https://gitlab.com/jcbmln/dotfiles/raw/master/install.sh`"
```

## License

The code is available under the [MIT license](LICENSE).
